fibLoop := method (n,
    a := 0
    b := 1
    c := 0
    d := 0

    for(i,0,n, c=b; d=a+b; a=c; b=d)

    return a
)
