class Tree
  attr_accessor :node_name, :children

  def initialize(name, children=[])
    @node_name = name

    if children.respond_to? :each_pair
      @children = []
      children.each_pair do |child_name, child_tree|
        @children << Tree.new( child_name, child_tree )
      end
    else 
      @children = children
    end
  end

  def visit_all(&block)
    visit &block
    children.each {|c| c.visit_all &block}
  end

  def visit(&block)
    block.call self
  end
end

tree = {
  'dad' => {
    'child 1' => {},
    'child 2' => {},
  },
  'uncle' => {
    'child 3' => {},
    'child 4' => {},
  },
}
ruby_tree = Tree.new 'grandpa', tree

ruby_tree.visit_all {|node| puts node.node_name}
