def matching_lines(filename, phrase)
  matches = []

  File.open(filename, 'r') do |f|
    phrase_regex = Regexp.new(Regexp.escape(phrase))
    f.readlines.each_with_index  do |line, lineno|
      if phrase_regex =~ line
        matches << { :line => lineno, :match => line.chomp }
      end
    end
  end

  matches
end

filename="/usr/share/dict/words"
phrase = "donkey"

matches = matching_lines filename, phrase
puts matches.to_s

