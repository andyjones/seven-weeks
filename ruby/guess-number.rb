def gen_rand_num
  rand(9)+1
end

def quit
  puts "So long sucka"
  exit
end

def play_game_once
  puts "I'm thinking of a number"
  target = gen_rand_num
  loop do
    puts "Guess a number between 1 and 10"
    input = gets.chomp
    if input.downcase == 'q'
      quit
    end

    guess = input.to_i

    if guess == target
      puts "Correct! I was thinking of #{target}"
      puts
      break 
    elsif guess <= 0 || guess > 10
      puts "Okay wiseass, read the instructions"
      puts "I'm thinking of a number between 1 and 10"
      puts "So I can't be thinking of #{guess}"
    elsif guess > target
      puts "Not quite, your guess is too high"
    elsif guess < target
      puts "Not quite, your guess is too low"
    else
      puts "Sorry, I have no idea want '#{guess}' is"
    end
  end
end

puts "Press CTRL-C to quit"
loop do
  play_game_once
end
