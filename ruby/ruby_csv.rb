class CsvRow
  attr_accessor :cols, :csv

  def initialize (cols=[], csv)
    @cols = cols
    @csv  = csv
  end

  def method_missing name, *args
    heading = name.to_s
    heading_idx = @csv.headers.find_index(heading)
    if heading_idx == nil
      raise NoMethodError.new(heading)
    end

    @cols[heading_idx]
  end
end

module ActsAsCsv
  def self.included(base)
    base.extend ClassMethods
  end

  module ClassMethods
    def acts_as_csv
      include InstanceMethods
    end
  end

  module InstanceMethods
    def read
      @csv_contents = []
      filename = self.class.to_s.downcase + '.txt'
      file = File.new(filename)
      @headers = file.gets.chomp.split(', ')

      file.each do |row|
        @csv_contents << row.chomp.split(', ')
      end
    end

    attr_accessor :headers, :csv_contents

    def initialize
      read
    end

    def each(&block)
      @csv_contents.each do |row|
        block.call CsvRow.new(row, self)
      end
    end

  end
end

class RubyCsv
  include ActsAsCsv
  acts_as_csv
end

m = RubyCsv.new
puts m.headers.inspect
puts m.csv_contents.inspect

m.each {|row| puts row.two }
